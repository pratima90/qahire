import os, sys
sys.path.insert(0, os.path.abspath(".."))
from Base import baseForLogin
import time

class InvalidUsernameInvalidPassword(baseForLogin.LoggedInBaseTestCase):
   
    def test_invalidUsernameInvalidPassword(self):
        
        self._user = "admin123"
        self._password = "Test@1234"
        driver = self.driver
       
        self.login()
        time.sleep(10)
        self.assertEqual(driver.current_url, "https://www.hudl.com/login")