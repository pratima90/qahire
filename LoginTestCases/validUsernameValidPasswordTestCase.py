import os, sys
sys.path.insert(0, os.path.abspath(".."))
from Base import baseForLogin
import time

class InvalidUsernameInvalidPassword(baseForLogin.LoggedInBaseTestCase):
   
    def test_invalidUsernameInvalidPassword(self):
        
        self._user = "pratima.lingden21@gmail.com"
        self._password = "test@123"
        driver = self.driver
       
        self.login()
        time.sleep(15)
        self.assertEqual(driver.current_url, "https://www.hudl.com/home")