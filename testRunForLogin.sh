echo "Running all test cases of login"

cd ~

echo "Entering to login folder"
cd Documents/SeleniumTest/LoginTestCases

echo "1. Invalid Username Invalid Password"
python -m unittest invalidUsernameInvalidPasswordTestCase

echo "2. Invalid Username Valid Password"
python -m unittest invalidUsernameValidPasswordTestCase

echo "3. Valid Username Invalid Password"
python -m unittest validUsernameInvalidPasswordTestCase

echo "4. Valid Username Valid Password"
python -m unittest validUsernameValidPasswordTestCase

echo "....................................."
echo "Completed all the testcases for login"
echo "....................................."
