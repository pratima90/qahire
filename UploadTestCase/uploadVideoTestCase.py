import os, sys
sys.path.insert(0, os.path.abspath(".."))
from Base import baseForLogin
import time

# Test To upload video and share with team member with success
class UploadVideo(baseForLogin.LoggedInBaseTestCase):
   
    def test_upload_video(self):
        
        self._user = "pratima.lingden21@gmail.com"
        self._password = "test@123"
        driver = self.driver
        self.login()

        time.sleep(5)
        driver.find_element_by_xpath('//*[@id="ssr-webnav"]/div/div[1]/nav[1]/div[4]/a[1]/span').click()
        input_video = driver.find_element_by_xpath('//*[@id="web-uploader-app"]/div/section/div/div/div/div/div[2]/div[2]/div[1]/input')
        input_video.send_keys("C:/Users/pratima/Downloads/test.mp4")

        time.sleep(15)
        driver.find_element_by_xpath('//*[@id="video-name__text-input"]').send_keys("Football Game Video uploading test")
        driver.find_element_by_xpath('//*[@id="web-uploader-app"]/div/section/div/div/section/div[1]/div[2]/div[5]/div/div/div[2]/input').send_keys('For Testing')
        driver.find_element_by_xpath('//*[@id="web-uploader-app"]/div/section/div/div/section/div[1]/div[2]/div[5]/div/div/div[2]').click()
        time.sleep(5)

        driver.find_element_by_xpath('//*[@id="web-uploader-app"]/div/section/div/div/section/div[1]/div[2]/div[6]/div/button').click()
        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="permissions-details"]/div[1]/button').click()
        time.sleep(2)
        
        driver.find_element_by_class_name("uni-select__input").click()
        driver.find_element_by_class_name("uni-select__input").send_keys('Sisir Joshi')
        driver.find_element_by_class_name('uni-select__item').click()
        driver.find_element_by_xpath('/html/body/div[9]/div/div/div[3]/div/button').click()
        time.sleep(5)

        