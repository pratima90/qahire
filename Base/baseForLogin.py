from Base import base

class LoggedInBaseTestCase(base.BaseTestCase):

    _user = None
    _password = None

    def login(self):
        driver = self.driver
        driver.get(self._baseUrl  + "/")

        driver.find_element_by_xpath('//*[@id="nav"]/ul/li[6]/a').click()
        driver.implicitly_wait(50)

        driver.find_element_by_id("email").clear()
        driver.implicitly_wait(50)
        driver.find_element_by_id("email").send_keys(self._user)

        driver.find_element_by_id("password").clear()
        driver.implicitly_wait(50)
        driver.find_element_by_id("password").send_keys(self._password)

        driver.find_element_by_id("logIn").click()


