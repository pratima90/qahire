import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By

class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(30)
        self._baseUrl = "https://hudl.com"

    # def tearDown(self):
    #     print(self.driver.current_url)
    #     # self.driver.close()


